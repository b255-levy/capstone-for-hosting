import React, { useEffect, useState } from 'react';


const ProductDetail = ({ product, onAddToCart, onBack }) => (
  <div>
    <h2>{product.name}</h2>
    <p>{product.description}</p>
    <p>{product.price}</p>
    <button onClick={() => onAddToCart(product._id)}>Checkout</button>
    <button onClick={onBack}>Back</button>
  </div>
);


const UserProducts = () => {
  const [products, setProducts] = useState([]);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [cart, setCart] = useState([]);
  const [showInactive, setShowInactive] = useState(false);

  const handleViewDetails = (product) => {
    setSelectedProduct(product);
  };

  const handleBack = () => {
    setSelectedProduct(null);
  };

  useEffect(() => {
    fetch('http://localhost:4000/users/products', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error(`HTTP error! status: ${response.status}`);
        }
        return response.json();
      })
      .then((data) => {
        if (Array.isArray(data)) {
          setProducts(data);
        } else {
          console.error('Unexpected data structure:', data);
        }
      })
      .catch((error) => {
        console.error('Fetch error:', error);
      });
  }, []);

  const handleAddToCart = async (productId) => {
    const token = localStorage.getItem('authToken');

    // Fetch user details to check if the user is an admin
    const userDetailsResponse = await fetch('http://localhost:4000/users/details', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    if (!userDetailsResponse.ok) {
      throw new Error(`HTTP error! status: ${userDetailsResponse.status}`);
    }

    const userDetails = await userDetailsResponse.json();
    if (userDetails.isAdmin) {
      alert('Admins cannot add products to the cart.');
      return;
    }

    // User is not an admin, proceed with adding product to cart
    try {
      const response = await fetch('http://localhost:4000/users/checkout', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          cart: [{ productId, quantity: 1 }],
        }),
      });
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const data = await response.json();
      setCart(data.cart); 
      alert('Product added to cart successfully!'); 
    } catch (error) {
      console.error('Add to cart error:', error);
    }
  };


  const checkout = async () => {
    const token = localStorage.getItem('authToken');
    try {
      const response = await fetch('http://localhost:4000/users/checkout', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({ cart }),
      });

      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }

      alert('Checkout successful!'); // Notify user
      setCart([]); // Clear cart after successful checkout
    } catch (error) {
      console.error('Checkout error:', error);
      alert('Checkout failed!'); // Notify user
    }
  };

  const handleToggleInactive = () => {
    setShowInactive(!showInactive);
  };

    return (
      <div>
        <button onClick={handleToggleInactive}>
          {showInactive ? 'Hide Inactive Products' : 'Show Inactive Products'}
        </button>
        {selectedProduct ? (
          <ProductDetail product={selectedProduct} onAddToCart={handleAddToCart} onBack={handleBack} />
        ) : (
        products
          .filter((product) => showInactive || product.isActive)
          .map((product) => (
            <div key={product._id}>
              <h2>{product.name}</h2>
              <button onClick={() => handleViewDetails(product)}>Details</button>
            </div>
          ))
        )}
      </div>
    );
  };

  export default UserProducts;
