import Container from 'react-bootstrap/Container';
import { useContext } from 'react';
import React, { useEffect, useState } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink, useNavigate } from 'react-router-dom';
import UserContext from './UserContext';

export default function AppNavBar() {
  const { user, setUser } = useContext(UserContext);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    console.log(user); 
    if(user && user.id){
      setIsLoggedIn(true);
    } else {
      setIsLoggedIn(false);
    }
  }, [user]);

  const handleLogout = () => {
    localStorage.removeItem('authToken');
    setUser({ id: null, isAdmin: false });
    setIsLoggedIn(false);
    navigate('/login');
  }

  return (
    <Navbar bg="light" expand="lg">
      <Container fluid>
        <Navbar.Brand as={Link} to="/">My App</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link as={NavLink} to="/home">Home</Nav.Link> {/* Modified the to prop */}
            <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
            {isLoggedIn ?
              <React.Fragment>
                <Nav.Link onClick={handleLogout}>Logout</Nav.Link> {/* Removed as={NavLink} */}
                {user.isAdmin && <Nav.Link as={NavLink} to="/admin">Admin Dashboard</Nav.Link>}
              </React.Fragment>
              :
              <React.Fragment>
                <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
              </React.Fragment>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}