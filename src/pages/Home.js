import React, { useEffect, useState } from 'react';

const HomePage = () => {
  const [userData, setUserData] = useState(null);

  useEffect(() => {
    fetch('http://localhost:4000/users/details', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('authToken')}`,
      },
    })
    .then(response => {
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      return response.json();
    })
    .then(user => setUserData(user))
    .catch(error => console.error('Fetch error:', error));
  }, []);

  const handleCheckout = async () => {
    try {
      const response = await fetch('http://localhost:4000/users/deleteAllOrders', {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('authToken')}`,
        },
      });

      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }

      alert('Checkout successful! All orders have been deleted.'); 
      setUserData(prevState => ({ ...prevState, orderedProducts: [] })); 
    } catch (error) {
      console.error('Checkout error:', error);
      alert('Checkout failed!');
    }
  };

  if (!userData) {
    return <div>Loading...</div>;
  }

  const totalAmount = userData.orderedProducts.reduce((total, order) => total + order.totalAmount, 0);

  return (
    <div>
      <h2>Welcome, {userData.email}</h2>
      <p>Your orders:</p>
      {userData.orderedProducts.map((order, index) => (
        <div key={index}>
          <h3>Order {index + 1}</h3>
          <p>Total Amount: {order.totalAmount}</p>
          <p>Purchased On: {new Date(order.purchasedOn).toLocaleDateString()}</p>
          <h4>Products:</h4>
          {order.products.map((product, index) => (
            <div key={index}>
              <p>Name: {product.productName}</p>
              <p>Quantity: {product.quantity}</p>
            </div>
          ))}
        </div>
      ))}
      <h2>Total: {totalAmount}</h2>
      <button onClick={handleCheckout}>Checkout</button>
    </div>
  );
};

export default HomePage;
