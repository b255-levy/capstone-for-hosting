import React, { useState, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import useAuth from '../useAuth';

function AdminDashboard() {
  const [products, setProducts] = useState([]);
  const [productName, setProductName] = useState('');
  const [productDescription, setProductDescription] = useState('');
  const [productPrice, setProductPrice] = useState('');
  const [error, setError] = useState('');
  const isAuthenticated = useAuth();
  const [productId, setProductId] = useState("");
  const [showUpdateForm, setShowUpdateForm] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);

  useEffect(() => {
    if (isAuthenticated) {
      fetchProducts();
    }
  }, [isAuthenticated]);

  if (!isAuthenticated) {
    return <Navigate to="/admin" />;
  }

  const fetchProducts = async () => {
    try {
      const response = await fetch('http://localhost:4000/users/products');
      const data = await response.json();

      setProducts(data);
      console.log(data);

    } catch (error) {
      setError(error.message);
    }
  };

  const handleFormSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await fetch('http://localhost:4000/users/products', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('authToken')}`,
        },
        body: JSON.stringify({ name: productName, price: productPrice, description: productDescription }),
      });
      await response.json();
      setProductName('');
      setProductPrice('');
      setProductDescription('');
      fetchProducts();
    } catch (error) {
      setError(error.message);
    }
  };

  const handleUpdateFormSubmit = async (event) => {
    event.preventDefault();

    const token = localStorage.getItem('token');

    try {
      const response = await fetch(`http://localhost:4000/users/products/${productId}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('authToken')}`,
        },
        body: JSON.stringify({ name: productName, price: productPrice, description: productDescription }),
      });


      const data = await response.json();
      if (!response.ok) {
        throw new Error(data.message || 'Failed to update product.');
      }

      setProducts(
        products.map((product) => (product._id === productId ? data.product : product))
      );
      setShowUpdateForm(false);
    } catch (error) {
      setError(error.message);
    }
  };

  const showUpdateProductForm = (product) => {
    setProductName(product.name);
    setProductPrice(product.price);
    setProductDescription(product.description);
    setProductId(product._id);
    setShowUpdateForm(true);
  };


  const toggleProductStatus = async (productId, currentStatus) => {
    try {
      const response = await fetch(`http://localhost:4000/users/products/${productId}/archive`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('authToken')}`,
        },
        body: JSON.stringify({ isActive: !currentStatus }),
      });
      await response.json();
      fetchProducts();
    } catch (error) {
      setError(error.message);
    }
  };



  return (
    <div className="container">
      <h2>Admin Dashboard</h2>
      <form onSubmit={handleFormSubmit} className="mb-4">
        <input
          type="hidden"
          id="productId"
          value={productId}
        />
        <div className="mb-3">
          <label htmlFor="productName" className="form-label">
            Product Name:
          </label>
          <input
            type="text"
            id="productName"
            className="form-control"
            value={productName}
            onChange={(event) => setProductName(event.target.value)}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="productDescription" className="form-label">
            Product Description:
          </label>
          <input
            type="text"
            id="productDescription"
            className="form-control"
            value={productDescription}
            onChange={(event) => setProductDescription(event.target.value)}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="productPrice" className="form-label">
            Product Price:
          </label>
          <input
            type="number"
            step="0.01"
            id="productPrice"
            className="form-control"
            value={productPrice}
            onChange={(event) => setProductPrice(event.target.value)}
            required
          />
        </div>
        <button type="submit" className="btn btn-primary">
          {productId ? "Update Product" : "Create Product"}
        </button>
      </form>

      {error && <p>{error}</p>}
      <h3>All Products</h3>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {Array.isArray(products) &&
            products.map((product) => (
              product && (
                <tr key={product._id}>
                  <td>{product.name}</td>
                  <td>{product.description}</td>
                  <td>{product.price}</td>
                  <td>{product.isActive ? "Active" : "Inactive"}</td>
                  <td>
                    <button
                      className={`btn btn-${product.isActive ? "danger" : "success"} me-2`}
                      onClick={() => toggleProductStatus(product._id, product.isActive)}
                    >
                      {product.isActive ? "Deactivate" : "Activate"}
                    </button>
                    <button className="btn btn-info" onClick={() => showUpdateProductForm(product)}>
                      Update
                    </button>
                  </td>
                </tr>
              )
            ))}
        </tbody>
      </table>
      {showUpdateForm && (
        <form onSubmit={handleUpdateFormSubmit} className="mb-3">
          <h3>Update Product Information</h3>
          <div className="mb-3">
            <label htmlFor="productName" className="form-label">
              Product Name
            </label>
            <input
              type="text"
              className="form-control"
              id="productName"
              value={productName}
              onChange={(e) => setProductName(e.target.value)}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="productDescription" className="form-label">
              Product Description
            </label>
            <input
              type="text"
              className="form-control"
              id="productDescription"
              value={productDescription}
              onChange={(e) => setProductDescription(e.target.value)}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="productPrice" className="form-label">
              Product Price
            </label>
            <input
              type="number"
              className="form-control"
              id="productPrice"
              value={productPrice}
              onChange={(e) => setProductPrice(e.target.value)}
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Update Product
          </button>
          <button
            type="button"
            className="btn btn-secondary ms-2"
            onClick={() => setShowUpdateForm(false)}
          >
            Cancel
          </button>
        </form>
      )}
    </div>
  );
}



export default AdminDashboard;