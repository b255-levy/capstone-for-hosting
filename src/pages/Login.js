import React, { useState, useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from './UserContext';

function LoginPage() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const { user, setUser } = useContext(UserContext);

  useEffect(() => {
    console.log(user);
  }, [user]);


  const handleFormSubmit = async (event) => {
    event.preventDefault();
    console.log('Submit button pressed');
    try {
      const response = await fetch('http://localhost:4000/users/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, password }),
      });

      console.log('Response received', response);

      if (!response.ok) {
        throw new Error(`Error ${response.status}: ${response.statusText}`);
      }

      const data = await response.json();
      console.log('Data received', data); 

      if (data && data.token) {
        localStorage.setItem('authToken', data.token);
        setIsLoggedIn(true);
        setUser({ id: data.user._id, isAdmin: data.user.isAdmin });
        window.alert('Successfully logged in!');
      }
    } catch (error) {
      console.error('Error occurred', error); 
      setError(error.message);
    }
  };


  if (isLoggedIn) {
    return <Navigate to="/products" />;
  }

  return (
    <div>
      <h2>Login Page</h2>
      <form onSubmit={handleFormSubmit}>
        <label>
          Email:
          <input
            type="email"
            name="email"
            value={email}
            onChange={(event) => setEmail(event.target.value)}
            required
          />
        </label>
        <br />
        <label>
          Password:
          <input
            type="password"
            name="password"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
            required
          />
        </label>
        <br />
        <button type="submit">Login</button>
      </form>
      {error && <p>{error}</p>}
    </div>
  );
}

export default LoginPage;
